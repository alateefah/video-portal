describe('VideoPortal', function () {
    var scope, controller, md5,location;
		
    beforeEach(function () { module('Authentication');  });
    beforeEach(function () { module('ngMd5');  });
    beforeEach(function () { module('ngCookies');  });
    
    describe('LoginController Test: ', function () {
        beforeEach(angular.mock.inject(function ($rootScope, $controller, _$location_, _md5_) {
            scope = $rootScope.$new();        
            location =  _$location_;
            md5 = _md5_;
            spyOn(location, 'path');
            controller = $controller('LoginController', {
                '$scope': scope,
                '$md5': md5,
                '$location': location
            });
        }));       
	
        /* This test that username passed is correct
         * ali = ali
         */
        it('should test the username passed to login function', function () {
                scope.username = "ali";
                scope.password = "password"
                scope.login();
                expect(scope.username).toBe("ali");
        });	
	    
        /* This test that the password passed login 
         * should be hashed. 
         */
        it('should test the password passed to login user is not hashed', function () {
                scope.username = "ali";
                scope.password = "password"; //unhashed password
                scope.login();
                expect(scope.password).not.toBe("password");
        });
        
        /* This test that the password passed to login is hashed
         * Password set is password. 
         * Password passed to log in is hashed password 
         */
        it('should test the password passed to login user is hashed', function () {
                scope.username = "ali";
                scope.password = "password"; //hashed password
                scope.login();
                expect(scope.password).toBe(md5.createHash('password'));
        });
	
    });    
    
});

