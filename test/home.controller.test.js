describe('VideoPortal', function () {
    
    var scope, controller;
		
    beforeEach(function () { module('Home');  });

    describe('HomeController Test: ', function () {
        beforeEach(inject(function ($rootScope, $controller) {
            scope = $rootScope.$new();
            controller = $controller('HomeController', {
                '$scope': scope
            });
        }));
        
        it('checks that initial user rate is 0', function () {
            expect(scope.initialRate).toEqual(0);
        });
        
        it('checks that the Average of [2,3,4,5] gives 3.5', function () {
            expect(scope.averageRate([2,3,4,5])).toEqual(3.5);
        });

    });
});