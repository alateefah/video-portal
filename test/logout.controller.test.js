/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

describe('VideoPortal', function () {
    var scope, controller, location,cookieStore;
		
    beforeEach(function () { module('Logout');  });
    beforeEach(function () { module('ngCookies');  });
    
    describe('LogoutController Test: ', function () {
        beforeEach(inject(function ($rootScope, $cookieStore, $controller, _$location_) {
            scope = $rootScope.$new();        
            location =  _$location_;
            spyOn(location, 'path');
            cookieStore = $cookieStore;
            controller = $controller('LogoutController', {
                '$scope': scope,
                '$location': location,
                '$cookieStore':cookieStore
            });
        }));
    });    
    
});