'use strict';
  
angular.module('Logout')
  
.factory('LogoutService',
    ['$http',
    function ($http) {
        var service = {};
        
        service.Logout = function (sessionId, callback) {
            $http.get('/user/logout?sessionId='+sessionId)
                .success(function (response) {
                    if(response.success === "success") {
                        response.message = response.error;
                    }
                    callback(response);
                });
        };        
        
        return service;
    }]);