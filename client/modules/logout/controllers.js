'use strict';
  
angular.module('Logout')
  
.controller('LogoutController',
    ['$scope', '$rootScope','$location', 'LogoutService',
    function ($scope,$rootScope, $location, LogoutService) {
        
        $scope.logout = function () {
            
            var sessionId = $rootScope.globals.currentUser.sessionId;
            
            /*
             * Pass sessionId to Logout service 
             */
            LogoutService.Logout(sessionId, function(response) {
                if(response.status === 'success') {                    
                    $location.path('/login');
                } else {
                    $scope.error = response.message;                   
                }
            });
        };
    }]);