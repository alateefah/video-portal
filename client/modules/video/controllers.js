'use strict';

angular.module('Video')

.controller('VideoController',
    ['$scope', '$rootScope', 'VideoService', 'videoId', 'HomeService',
    function ($scope, $rootScope, VideoService, videoId, HomeService) {         
        $scope.position = "";
        
        $scope.getVideo = function() {
            if ($rootScope.globals.currentUser) {
                var sessionId = $rootScope.globals.currentUser.sessionId; 
                $scope.dataLoading = true;    
                VideoService.fetchVideo(sessionId, videoId, function(response) {  
                    if(response.status === 'success') {
                        $scope.video_id = response.data._id; 
                        $scope.name = response.data.name; 
                        $scope.ratings = response.data.ratings; 
                        $scope.url = response.data.url; 
                        $scope.description = response.data.description; 
                        $scope.averageRating = HomeService.calculateAverageRating(response.data.ratings);    
                        $scope.position = response.data.name.match(/\[(.*?)\]/)[1];
                        console.log($scope.position);
                    } else {
                        $scope.error = response.message;                       
                    }
                    $scope.dataLoading = false;
                });
            } else {
                $scope.error = "User not logged in";
                $scope.dataLoading = false;
            }            
        }; 
        
        $scope.rateVideo = function(videoId, rate) {
            if ($rootScope.globals.currentUser) {
                var sessionId = $rootScope.globals.currentUser.sessionId; 
                $scope.dataLoading = true;    
                HomeService.rate(sessionId, videoId, rate, function(response) {
                    if(response.status === 'success') {
                        $scope.getVideo();           
                        $.toast({
                            text: "You rated<br/>Video: <b>"+response.data.name.substr(3)+"</b><br/>Rate: "+rate+"  ",
                            hideAfter: false
                        });
                    } else {
                        $scope.error = response.message;
                        $scope.dataLoading = false;
                    }
                });
            } else {
                $scope.error = "User not logged in";
                $scope.dataLoading = false;
            }     
        };
        
        
        
        $scope.getSomeVideos = function() {
            $scope.skip = parseInt($scope.position)+1;       
            $scope.limit = 3;
            
            if ($rootScope.globals.currentUser) {
                var sessionId = $rootScope.globals.currentUser.sessionId; 
                //$scope.dataLoading = true;    
                HomeService.fetchVideos(sessionId, $scope.limit, $scope.skip, function(response) {           
                    if(response.status === 'success') {
                        $scope.someVideos = response.data;                        
                    } else {
                        $scope.error = response.message;
                        $scope.dataLoading = false;
                    }
                });
            } else {
                $scope.error = "User not logged in";
                $scope.dataLoading = false;
            }
        };
        
    }]);
