'use strict';
  
angular.module('Video')

.factory('VideoService',
    ['$http',
    function ($http) {
        var service = {};
 
        //get a video
        service.fetchVideo = function (sessionId, videoId, callback) {
            $http.get('/video?sessionId='+sessionId+'&videoId='+videoId)
                .success(function (response) {
                    if(response.status !== "success") {
                        response.message = response.error;
                    }
                    callback(response);
                });
 
        };     
        
        return service;
    }]);