'use strict';
  
angular.module('Authentication')
  
.controller('LoginController',
    ['$scope', '$rootScope','md5', '$location', 'AuthenticationService',
    function ($scope,$rootScope, md5, $location, AuthenticationService) {
        // reset login status
        
        //$scope.loginStatus = false;
        
        AuthenticationService.ClearCredentials();
        
        $scope.login = function () {
            $scope.password = md5.createHash($scope.password);
            $scope.dataLoading = true;
            AuthenticationService.Login($scope.username, $scope.password, function(response) {                
                if(response.status === 'success') {
                    $scope.loginStatus = true;
                    AuthenticationService.SetCredentials(response.username, response.sessionId);
                    $location.path('/');
                } else {
                    $scope.loginStatus = false;
                    $scope.error = response.message;
                    $scope.dataLoading = false;
                }
            });
        };       
 
    }]);