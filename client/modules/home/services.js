'use strict';
  
angular.module('Home')

.factory('HomeService',
    ['$http',
    function ($http) {
        var service = {};
 
        //get vidoes from api
        service.fetchVideos = function (sessionId, limit, skip, callback) {
            $http.get('/videos?sessionId='+sessionId+'&limit='+limit+'&skip='+skip)
                .success(function (response) {
                    if(response.status !== "success") {
                        response.message = response.error;
                    }
                    callback(response);
                });
 
        };
        
        //rate a video
        service.rate = function (sessionId, videoId, rate, callback) {
            $http.post('/video/ratings?sessionId='+sessionId, {videoId:videoId, rating: rate})
                .success(function (response) {
                    console.log(response);
                    if(response.status !== "success") {
                        response.message = response.error;
                    }
                    callback(response);
                });
 
        };
        
        //calculate average rating
        service.calculateAverageRating = function (ratings) { 
            var sum = 0; 
            for (var i = 0; i < ratings.length; i++) {
                sum += parseInt(ratings[i], 10); //don't forget to add the base 
            }
            var avg = sum/ratings.length;
            return avg; 
        };
        
        return service;
    }]);