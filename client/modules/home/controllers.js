'use strict';

angular.module('Home')

.controller('HomeController',
    ['$scope', '$rootScope', 'HomeService',
    function ($scope, $rootScope, HomeService) {
        
        $scope.initialRate = 0;        
        
        //return some vidoeos with limit and skip        
        $scope.getVideos = function(limit, skip) {
            if ($rootScope.globals.currentUser) {
                var sessionId = $rootScope.globals.currentUser.sessionId; 
                $scope.dataLoading = true;    
                HomeService.fetchVideos(sessionId, limit, skip, function(response) {           
                    if(response.status === 'success') {                        
                        $scope.videos = response.data; 
                    } else {
                        $scope.error = response.message;
                        $scope.dataLoading = false;
                    }
                });
            } else {
                $scope.error = "User not logged in";
                $scope.dataLoading = false;
            }
        };
        
        /*
         * Calculate average rating from array of values
         */
        $scope.averageRate = function(ratings){             
            $scope.average = HomeService.calculateAverageRating(ratings);       
            return $scope.average;
        };
        
        
        $scope.playerReady = function (api) {
            $scope.api = api;
        };
        
        $scope.stateChange = function (state) {
            if (state === 'play') {
                if($rootScope.playingVideo && $rootScope.plsyingVideo !== $scope.api)
                    $rootScope.playingVideo.pause();
                $rootScope.playingVideo = $scope.api;
            }
        };
        
        $scope.hoveringOver = function(value) {
            $scope.overStar = value;
            $scope.percent = 100 * (value / $scope.max);
        };

        $scope.ratingStates = [
            {stateOn: 'glyphicon-ok-sign', stateOff: 'glyphicon-ok-circle'},
            {stateOn: 'glyphicon-star', stateOff: 'glyphicon-star-empty'},
            {stateOn: 'glyphicon-heart', stateOff: 'glyphicon-ban-circle'},
            {stateOn: 'glyphicon-heart'},
            {stateOff: 'glyphicon-off'}
        ];
        
        $scope.rateVideo = function(videoId, rate){
            if ($rootScope.globals.currentUser) {
                var sessionId = $rootScope.globals.currentUser.sessionId; 
                $scope.dataLoading = true;    
                HomeService.rate(sessionId, videoId, rate, function(response) {
                    if(response.status === 'success') {
                        $scope.getVideos(); 
                        $.toast({
                            text: "You rated<br/>Video: <b>"+response.data.name.substr(3)+"</b><br/>Rate: "+rate+"  ",
                            hideAfter: false
                        });
                                               
                    } else {
                        $scope.error = response.message;
                        $scope.dataLoading = false;
                    }
                });
            } else {
                $scope.error = "User not logged in";
                $scope.dataLoading = false;
            }     
        };
    }]);