'use strict';


// declare modules
angular.module('Authentication', []);   //login module
angular.module('Home', []);             //Video listing module 
angular.module('Logout', []);           //logout module
angular.module('Video', []);            //single vido module
 
angular.module('CrossOverVideoPortal', [
    'Authentication',
    'Home',
    'Logout',
    'Video',
    'ngMd5',
    'ngRoute',
    'ngCookies',
    'infinite-scroll',
    'ngSanitize',
    'com.2fdevs.videogular',
    'ui.bootstrap'
])
  
.config(['$routeProvider', function ($routeProvider) {
 
    $routeProvider
        .when('/login', {
            controller: 'LoginController',
            templateUrl: 'modules/authentication/views/login.html'
        })
  
        .when('/logout', {
            controller: 'LogoutController',
            templateUrl: '#'
        })
        
        .when('/', {
            controller: 'HomeController',
            templateUrl: 'modules/home/views/home.html'
        })
        
        .when('/watch/:v', {
            controller: 'VideoController',
            templateUrl: 'modules/video/views/video.html',
            resolve: {
                videoId: function( $route ) {           //return video id
                    return ($route.current.params.v).substr(2);
                }
            }
        })
        
        .otherwise({ redirectTo: '/login' });
}])
  
.run(['$rootScope', '$location', '$cookieStore', '$http',
    function ($rootScope, $location, $cookieStore, $http) {
        // keep user logged in after page refresh
        $rootScope.globals = $cookieStore.get('globals') || {};
        if ($rootScope.globals.currentUser) {
            $http.defaults.headers.common['Authorization'] = 'Basic ' + $rootScope.globals.currentUser.sessionId; // jshint ignore:line
        }
  
        $rootScope.$on('$locationChangeStart', function (event, next, current) {
            // redirect to login page if not logged in
            if ($location.path() !== '/login' && !$rootScope.globals.currentUser) {
                $location.path('/login');
            }
        });
    }]);